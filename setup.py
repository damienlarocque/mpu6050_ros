import os
from glob import glob
from setuptools import find_packages, setup

package_name = "mpu6050_ros"

setup(
    name=package_name,
    version="0.0.2",
    packages=find_packages(exclude=["test"]),
    data_files=[
        ("share/ament_index/resource_index/packages", ["resource/" + package_name]),
        ("share/" + package_name, ["package.xml"]),
        # Include all launch files.
        (os.path.join("share", package_name), glob("launch/*launch.[pxy][yma]*")),
    ],
    install_requires=["setuptools", "adafruit-circuitpython-mpu6050"],
    zip_safe=True,
    maintainer="Damien LaRocque",
    maintainer_email="phicoltan@gmail.com",
    description="ROS 2 driver for the Adafruit MPU-6050 IMU",
    license="MIT",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": [
            "mpu6050_node = mpu6050_ros.mpu6050_node:main",
        ],
    },
)
