from launch_ros.actions import Node

from launch import LaunchDescription


def generate_launch_description():
    return LaunchDescription(
        [
            Node(
                package="mpu6050_ros",
                executable="mpu6050_node",
                name="mpu6050_node",
                parameters=[
                    {"frame_id": "imu_mpu6050"},
                    {"pub_rate": 50},
                ],
            )
        ]
    )
