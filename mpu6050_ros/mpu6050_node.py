import math
import busio
import board
import adafruit_mpu6050
import rclpy
import sensor_msgs.msg
from rclpy.node import Node


class MPU6050Node(Node):
    def __init__(self):
        super().__init__("mpu6050_node")

        # Logger
        self.logger = self.get_logger()

        # Parameters
        self.declare_parameter("frame_id", "imu_mpu6050")
        frame_id = self.get_parameter("frame_id").get_parameter_value().string_value
        self.frame_id = frame_id

        self.declare_parameter("pub_rate", 50)
        pub_rate = self.get_parameter("pub_rate").get_parameter_value().integer_value
        self.pub_rate = pub_rate

        # IMU instance
        i2c = busio.I2C(board.SCL, board.SDA)
        self.imu = adafruit_mpu6050.MPU6050(i2c)

        # Publishers
        self.imu_pub_ = self.create_publisher(sensor_msgs.msg.Imu, "/imu/data_raw", 10)
        self.pub_clk_ = self.create_timer(1 / self.pub_rate, self.publish_cback)

    def publish_cback(self):
        imu_msg = sensor_msgs.msg.Imu()

        imu_msg.header.stamp = self.get_clock().now().to_msg()
        imu_msg.header.frame_id = self.frame_id
        ax, ay, az = self.imu.acceleration
        gx, gy, gz = self.imu.gyro

        imu_msg.linear_acceleration.x = ax
        imu_msg.linear_acceleration.y = ay
        imu_msg.linear_acceleration.z = az
        imu_msg.angular_velocity.x = math.radians(gx)
        imu_msg.angular_velocity.y = math.radians(gy)
        imu_msg.angular_velocity.z = math.radians(gz)
        imu_msg.orientation_covariance[0] = -1

        self.imu_pub_.publish(imu_msg)


def main(args=None):
    rclpy.init(args=args)
    mpu6050_node = MPU6050Node()
    rclpy.spin(mpu6050_node)

    mpu6050_node.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
